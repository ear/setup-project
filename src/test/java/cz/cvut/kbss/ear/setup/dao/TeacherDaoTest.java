package cz.cvut.kbss.ear.setup.dao;

import cz.cvut.kbss.ear.setup.model.Teacher;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

public class TeacherDaoTest extends BaseDaoTestRunner {

    // We can use DI thanks to the Spring test container
    @Autowired
    private TeacherDao teacherDao;

    @Test
    public void findByNameFindsTeacherByFirstNameAndLastName() {
        final Teacher t = new Teacher();
        t.setFirstName("Severus");
        t.setLastName("Snape");
        t.setEmail("severus.snape@hogwarts.co.uk");
        t.setRoom("E-227");
        teacherDao.persist(t);

        final Teacher result = teacherDao.findByName(t.getFirstName(), t.getLastName());
        assertNotNull(result);
        assertEquals(t.getId(), result.getId());
    }
}