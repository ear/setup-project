package cz.cvut.kbss.ear.setup.model;

import javax.persistence.*;
import java.io.Serializable;

@NamedQueries({@NamedQuery(name = "Teacher.findAll", query = "SELECT t FROM Teacher t"),
               @NamedQuery(name = "Teacher.findByName",
                           query = "SELECT t FROM Teacher t WHERE t.firstName = :firstName AND t.lastName = :lastName")})
@Entity
public class Teacher implements Serializable {

    @Id
    @GeneratedValue
    private Integer id;

    @Basic(optional = false)
    @Column(nullable = false)
    private String firstName;

    @Basic(optional = false)
    @Column(nullable = false)
    private String lastName;

    @Basic(optional = false)
    @Column(nullable = false)
    private String email;

    @Basic(optional = false)
    @Column(nullable = false)
    private String room;

    public Teacher() {
    }

    public Teacher(String firstName, String lastName, String email, String room) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.room = room;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    @Override
    public String toString() {
        return "Teacher{" + firstName + ' ' + lastName + '}';
    }
}
