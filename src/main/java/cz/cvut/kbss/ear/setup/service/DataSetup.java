package cz.cvut.kbss.ear.setup.service;

import cz.cvut.kbss.ear.setup.model.Teacher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Component
public class DataSetup {

    private static final List<Teacher> TEACHERS = initTeachers();

    @Autowired
    private TeacherService teacherService;

    private static List<Teacher> initTeachers() {
        final List<Teacher> lst = new ArrayList<>();
        lst.add(new Teacher("Miroslav", "Blaško", "miroslav.blasko@fel.cvut.cz", "E-227"));
        lst.add(new Teacher("Bogdan", "Kostov", "bogdan.kostov@fel.cvut.cz", "E-225c"));
        lst.add(new Teacher("Martin", "Ledvinka", "martin.ledvinka@fel.cvut.cz", "E-227"));
        lst.add(new Teacher("Petr", "Křemen", "petr.kremen@fel.cvut.cz", "E-227"));
        return lst;
    }

    @PostConstruct
    private void setupData() {
        TEACHERS.stream().filter(t -> !teacherService.exists(t.getFirstName(), t.getLastName()))
                .forEach(t -> teacherService.persist(t));
    }
}
