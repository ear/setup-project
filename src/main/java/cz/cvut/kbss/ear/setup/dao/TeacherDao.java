package cz.cvut.kbss.ear.setup.dao;

import cz.cvut.kbss.ear.setup.model.Teacher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Objects;

@Repository
public class TeacherDao {

    private static final Logger LOG = LoggerFactory.getLogger(TeacherDao.class);

    @PersistenceContext
    private EntityManager em;

    public List<Teacher> findAll() {
        return em.createNamedQuery("Teacher.findAll", Teacher.class).getResultList();
    }

    public Teacher findByName(String firstName, String lastName) {
        Objects.requireNonNull(firstName);
        Objects.requireNonNull(lastName);
        try {
            return em.createNamedQuery("Teacher.findByName", Teacher.class).setParameter("firstName", firstName)
                     .setParameter("lastName", lastName).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public void persist(Teacher teacher) {
        Objects.requireNonNull(teacher);
        em.persist(teacher);
        LOG.debug("Successfully persisted teacher {}.", teacher);
    }
}
