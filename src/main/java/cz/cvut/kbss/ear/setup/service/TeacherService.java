package cz.cvut.kbss.ear.setup.service;

import cz.cvut.kbss.ear.setup.dao.TeacherDao;
import cz.cvut.kbss.ear.setup.model.Teacher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TeacherService {

    @Autowired
    private TeacherDao teacherDao;

    // Declarative transaction demarcation. readOnly set to true is good for retrieval operations, because it
    // may involve less locking on the lower layers
    // If the services are split into interfaces and implementations, the @Transactional annotation usually goes
    // to the interface, so that ic can apply to all possible implementations of the service
    @Transactional(readOnly = true)
    public List<Teacher> findAll() {
        return teacherDao.findAll();
    }

    @Transactional(readOnly = true)
    public boolean exists(String firstName, String lastName) {
        return teacherDao.findByName(firstName, lastName) != null;
    }

    // No readOnly here, because we modify the data in this operation
    @Transactional
    public void persist(Teacher teacher) {
        teacherDao.persist(teacher);
    }
}
