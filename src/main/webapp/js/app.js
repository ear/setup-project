import React from 'react';
import ReactDOM from 'react-dom';
import {PageHeader} from 'react-bootstrap';

import InfoMessage from './components/InfoMessage';
import TeachersController from './components/TeachersController';

const App = () => {
    return <div>
        <PageHeader>Your teachers</PageHeader>
        <TeachersController/>
        <InfoMessage/>
    </div>;
};

ReactDOM.render(<App/>, document.getElementById("content"));
