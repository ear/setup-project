import React from 'react';
import Reflux from "reflux";

import Actions from '../actions/Actions';
import Teachers from './Teachers';
import TeacherStore from '../stores/TeacherStore';

export default class TeachersController extends Reflux.Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.store = TeacherStore;
    }

    componentDidMount() {
        Actions.loadTeachers();
    }

    render() {
        return <Teachers teachers={this.state.teachers}/>;
    }
}
