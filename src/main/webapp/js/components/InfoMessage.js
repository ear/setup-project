import React from 'react';
import {Alert} from 'react-bootstrap';

const InfoMessage = () => {
    return <Alert bsStyle="success">If you are seeing this message, your system has all the necessary tools and
        libraries
        and is prepared for developing projects in the B6B33EAR course. <strong>Congratulations!</strong></Alert>;
};

export default InfoMessage;
