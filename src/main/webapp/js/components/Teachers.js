import React from 'react';
import PropTypes from "prop-types";
import {Panel, Table} from 'react-bootstrap';

class Teachers extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return <Panel header={<h5>Teachers</h5>} bsStyle="primary">
            <Table striped bordered hover condensed>
                <thead>
                <tr>
                    <th className="col-xs-3 content-center">First name</th>
                    <th className="col-xs-3 content-center">Last Name</th>
                    <th className="col-xs-3 content-center">Contact</th>
                    <th className="col-xs-2 content-center">Room</th>
                </tr>
                </thead>
                <tbody>
                {this._renderRows()}
                </tbody>
            </Table>
        </Panel>;
    }

    _renderRows() {
        const rows = [],
            teachers = this.props.teachers;
        for (let i = 0, len = teachers.length; i < len; i++) {
            rows.push(<Row key={'teacher-' + teachers[i].id} teacher={teachers[i]}/>);
        }
        return rows;
    }
}

Teachers.propTypes = {
    teachers: PropTypes.array.isRequired
};

const Row = (props) => {
    const teacher = props.teacher;
    return <tr>
        <td>{teacher.firstName}</td>
        <td>{teacher.lastName}</td>
        <td>{teacher.email}</td>
        <td className="content-center">{teacher.room}</td>
    </tr>;
};

Row.propTypes = {
    teacher: PropTypes.object.isRequired
};

export default Teachers;
