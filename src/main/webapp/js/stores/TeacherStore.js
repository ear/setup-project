import Reflux from 'reflux';
import request from 'superagent';

import Actions from '../actions/Actions';

const URL = 'rest/teachers';

export default class TeacherStore extends Reflux.Store {

    constructor() {
        super();
        this.state = {
            teachers: []
        };
        this.listenables = Actions;
    }

    onLoadTeachers() {
        request.get(URL).accept('json').end((err, resp) => {
            if (err) {
                console.log('Error when loading teachers. Status: ' + err.status);
            } else {
                this.setState({teachers: resp.body});
            }
        });
    }
}
