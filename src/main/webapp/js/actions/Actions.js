'use strict';

import Reflux from 'reflux';

const Actions = Reflux.createActions([
    'loadTeachers'
]);

export default Actions;
